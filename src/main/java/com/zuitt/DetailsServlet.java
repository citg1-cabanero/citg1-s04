package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;

	
	public void init() throws ServletException {
		
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
	ServletContext srvContext = getServletContext();
	
	String branding = srvContext.getInitParameter("branding");
	String fname = System.getProperty("firstname");

	HttpSession session = req.getSession();
	String lname = session.getAttribute("lastname").toString();
	String email = srvContext.getAttribute("email").toString();
	String contact = req.getParameter("contactnumber");
	
	PrintWriter output = res.getWriter();
	output.println(
			"<h1>" + branding + "</h1>" +
			"<p>First Name: " + fname + "</p>" +
			"<p>Last Name: " + lname + "</p>" +
			"<p>Contact Number: "+ contact +"</p>"+
			"<p>Email: " + email + "</p>" 
		);
	
	}
}


	
