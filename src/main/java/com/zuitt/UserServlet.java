package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8689152659639693816L;

	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		System.getProperties().put("firstname", req.getParameter("firstname"));
		
		
		HttpSession session = req.getSession();
		session.setAttribute("lastname", req.getParameter("lastname"));
		
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email",req.getParameter("email"));
		
		res.sendRedirect("details?contactnumber="+req.getParameter("contact"));
		
	}
	
	public void destroy() {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroyed. ");
		System.out.println("******************************************");
	}
	
}

